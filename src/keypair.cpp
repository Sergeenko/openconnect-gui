/*
 * Copyright (C) 2014, 2015 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "keypair.h"
#include "common.h"
#include <QInputDialog>

KeyPair::KeyPair(QWidget* p_window)
    : m_key(Key(p_window))
    , m_window(p_window)
    {}

void KeyPair::cleanup(gnutls_pkcs12_t p_pkcs12, gnutls_datum_t& p_rawData)
{
    if (p_rawData.data)
    {
        gnutls_free(p_rawData.data);
    }
    if (p_pkcs12)
    {
        gnutls_pkcs12_deinit(p_pkcs12);
    }
}

int KeyPair::import_pfx(const QString& p_fileName)
{
    if (not m_window or is_url(p_fileName))
    {
        return -1;
    }

    gnutls_pkcs12_t pkcs12 = nullptr;
    gnutls_datum_t raw = { nullptr, 0 };
    int ret = gnutls_load_file(p_fileName.toLatin1().data(), &raw);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        cleanup(pkcs12, raw);
        return -1;
    }

    /* check if the file data contain BEGIN PKCS12 */
    bool isPem = false;
    char* p = strstr((char*)raw.data, "--- BEGIN ");
    if (p)
    {
        isPem = true;
        if (not strstr(p, "--- BEGIN PKCS12"))
        {
            return -1;
        }
    }

    ret = gnutls_pkcs12_init(&pkcs12);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        cleanup(pkcs12, raw);
        return -1;
    }

    ret = gnutls_pkcs12_import(pkcs12, &raw,
        (isPem) ? GNUTLS_X509_FMT_PEM : GNUTLS_X509_FMT_DER,
        0);
    if (ret < 0) {
        last_err = gnutls_strerror(ret);
        cleanup(pkcs12, raw);
        return -1;
    }

    bool ok = false;
    QString pass = QInputDialog::getText(m_window,
        QLatin1String("This file requires a password"),
        QLatin1String("Please enter your password"),
        QLineEdit::Password,
        QString(),
        &ok);

    if (not ok)
    {
        cleanup(pkcs12, raw);
        return -1;
    }

    ret = gnutls_pkcs12_verify_mac(pkcs12, pass.toLatin1().data());
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        cleanup(pkcs12, raw);
        return -1;
    }

    gnutls_x509_privkey_t xkey;
    gnutls_x509_crt_t* xcert;
    unsigned int xcert_size;
    ret = gnutls_pkcs12_simple_parse(pkcs12, pass.toLatin1().data(), &xkey, &xcert,
        &xcert_size, nullptr, nullptr, nullptr, 0);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        cleanup(pkcs12, raw);
        return -1;
    }

    if (xkey)
    {
        m_key.set(xkey);
    }
    if (xcert_size > 0) {
        m_cert.set(xcert[0]);
    }

    for (unsigned i = 1; i < xcert_size; ++i)
    {
        gnutls_x509_crt_deinit(xcert[i]);
    }
    ret = 0;
    cleanup(pkcs12, raw);
    return ret;
}

int KeyPair::import_cert(const QString& File)
{
    int ret = m_cert.import_file(File);
    if (ret not_eq 0)
    {
        last_err = m_cert.last_err;
        return -1;
    }
    return 0;
}

int KeyPair::import_key(const QString& File)
{
    int ret = m_key.import_file(File);
    if (ret not_eq 0)
    {
        last_err = m_key.last_err;
        return -1;
    }
    return 0;
}

int KeyPair::cert_export(QByteArray& data)
{
    return m_cert.data_export(data);
}

int KeyPair::key_export(QByteArray& data)
{
    return m_key.data_export(data);
}

bool KeyPair::is_complete() const
{
    return m_key.IsImported and m_cert.IsImported;
}
