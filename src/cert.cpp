/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cert.h"
#include "common.h"
extern "C" {
#include <gnutls/pkcs11.h>
}

Cert::~Cert()
{
    clear();
}

void Cert::clear()
{
    if (m_cert) {
        gnutls_x509_crt_deinit(m_cert);
        m_cert = nullptr;
        IsImported = false;
    }
}

int Cert::import_cert(gnutls_datum_t* p_raw, bool p_isPem)
{
    if (p_raw->size == 0)
    {
        return -1;
    }

    gnutls_x509_crt_init(&m_cert);

    int ret = gnutls_x509_crt_import(m_cert, p_raw, GNUTLS_X509_FMT_PEM);
    if (not p_isPem
        and (ret == GNUTLS_E_BASE64_DECODING_ERROR or ret == GNUTLS_E_BASE64_UNEXPECTED_HEADER_ERROR))
    {
        ret = gnutls_x509_crt_import(m_cert, p_raw, GNUTLS_X509_FMT_DER);
    }
    if (ret < 0)
    {
        clear();
    }
    return ret;
}

int Cert::import_pem(QByteArray data)
{
    if (IsImported)
    {
        clear();
    }

    gnutls_datum_t raw;
    raw.data = (unsigned char*)data.constData();
    raw.size = data.size();

    int ret = import_cert(&raw, true);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return -1;
    }

    IsImported = true;
    return 0;
}

void Cert::set(gnutls_x509_crt_t crt)
{
    clear();
    m_cert = crt;
    IsImported = true;
}

int Cert::data_export(QByteArray& data)
{
    data.clear();

    if (not IsImported)
    {
        return -1;
    }

    gnutls_datum_t raw;
    int ret = gnutls_x509_crt_export2(m_cert, GNUTLS_X509_FMT_PEM, &raw);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return -1;
    }

    data = QByteArray((char*)raw.data, raw.size);
    gnutls_free(raw.data);
    return 0;
}

int Cert::import_file(const QString& p_fileName)
{
    if (p_fileName.isEmpty())
    {
        return -1;
    }

    if (IsImported)
    {
        clear();
    }

    if (is_url(p_fileName))
    {
        gnutls_x509_crt_init(&m_cert);

        int ret = gnutls_x509_crt_import_pkcs11_url(m_cert, p_fileName.toLatin1().data(), 0);
        if (ret < 0)
        {
            ret = gnutls_x509_crt_import_pkcs11_url(m_cert,
                p_fileName.toLatin1().data(),
                GNUTLS_PKCS11_OBJ_FLAG_LOGIN);
        }

        if (ret < 0)
        {
            last_err = gnutls_strerror(ret);
            return -1;
        }
        IsImported = true;
        return 0;
    }

    /* normal file */
    gnutls_datum_t contents = { nullptr, 0 };
    int ret = gnutls_load_file(p_fileName.toLatin1().data(), &contents);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return -1;
    }

    ret = import_cert(&contents, false);
    gnutls_free(contents.data);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return -1;
    }

    IsImported = true;
    return 0;
}

QString Cert::exportTmpFile() const
{
    gnutls_datum_t out;
    int ret = gnutls_x509_crt_export2(m_cert, GNUTLS_X509_FMT_PEM, &out);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return "";
    }

    QByteArray qa;
    qa.append((const char*)out.data, out.size);
    gnutls_free(out.data);

    m_tmpfile.resize(0);
    m_tmpfile.setFileTemplate("tmp-certXXXXXX");
    m_tmpfile.open();
    ret = m_tmpfile.write(qa);
    m_tmpfile.close();
    if (ret == -1)
    {
        return "";
    }

    return m_tmpfile.fileName();
}

QString Cert::sha1_hash() const
{
    if (not IsImported)
    {
        return "";
    }

    unsigned char id[32];
    size_t len = sizeof(id);
    int ret = gnutls_x509_crt_get_key_id(m_cert, 0, id, &len);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return "";
    }

    QByteArray array;
    array.append((const char*)id, len);
    return QObject::tr("SHA1:") + array.toHex();
}

