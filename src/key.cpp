/*
 * Copyright (C) 2014, 2015 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "key.h"
#include "common.h"
#include <QInputDialog>
extern "C" {
#include <gnutls/pkcs11.h>
}

Key::Key(QWidget* p_window)
    : m_window(p_window)
{}

Key::~Key()
{
    clear();
}

void Key::clear()
{
    if (m_privkey)
    {
        gnutls_x509_privkey_deinit(m_privkey);
        m_privkey = nullptr;
    }
    m_url.clear();
    IsImported = false;
}

int Key::importKey(const QByteArray& data)
{
    gnutls_datum_t raw;
    raw.data = (unsigned char*)data.constData();
    raw.size = data.size();

    if (raw.size == 0)
    {
        return -1;
    }

    gnutls_x509_privkey_init(&m_privkey);

    int ret = gnutls_x509_privkey_import2(m_privkey, &raw, GNUTLS_X509_FMT_PEM, NULL, 0);
    if (ret == GNUTLS_E_DECRYPTION_FAILED and m_window)
    {
        bool ok;
        QString text = QInputDialog::getText(m_window,
            QLatin1String("This file requires a password"),
            QLatin1String("Please enter your password"),
            QLineEdit::Password, QString(), &ok);
        if (not ok)
        {
            clear();
            return ret;
        }

        ret = gnutls_x509_privkey_import2(m_privkey, &raw, GNUTLS_X509_FMT_PEM,
            text.toLatin1().data(), 0);
    }

    if (ret == GNUTLS_E_BASE64_DECODING_ERROR or ret == GNUTLS_E_BASE64_UNEXPECTED_HEADER_ERROR)
    {
        ret = gnutls_x509_privkey_import(m_privkey, &raw, GNUTLS_X509_FMT_DER);
    }
    if (ret < 0)
    {
        clear();
    }

    return ret;
}

int Key::import_pem(QByteArray p_data)
{

    int ret = importKey(p_data);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return -1;
    }
    IsImported = true;
    return 0;
}

void Key::set(const gnutls_x509_privkey_t privkey)
{
    clear();
    m_privkey = privkey;
    IsImported = true;
}

int Key::data_export(QByteArray& data)
{
    if (not IsImported)
    {
        return -1;
    }

    data.clear();
    if (not m_url.isEmpty())
    {
        /* export the URL */
        data.append(m_url.toLatin1());
        return 0;
    }

    gnutls_datum_t raw;
    int ret = gnutls_x509_privkey_export2(m_privkey, GNUTLS_X509_FMT_PEM, &raw);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return -1;
    }

    data = QByteArray((char*)raw.data, raw.size);
    gnutls_free(raw.data);
    return 0;
}

int Key::import_file(const QString& p_fileName)
{
    gnutls_datum_t contents = { NULL, 0 };

    if (p_fileName.isEmpty())
    {
        return -1;
    }

    if (IsImported)
    {
        clear();
    }

    if (is_url(p_fileName))
    {
        m_url = p_fileName;
        IsImported = true;
        return 0;
    }

    /* normal file */
    int ret = gnutls_load_file(p_fileName.toLatin1().data(), &contents);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return -1;
    }

    ret = importKey(QByteArray((char*)contents.data, contents.size));
    gnutls_free(contents.data);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return -1;
    }

    IsImported = true;
    return 0;
}

QString Key::exportTmpFile() const
{
    if (not IsImported)
    {
        return "";
    }

    if (not m_url.isEmpty())
    {
        return m_url;
    }

    m_tmpfile.resize(0);
    m_tmpfile.setFileTemplate("tmp-keyXXXXXX");

    gnutls_datum_t out;
    int ret = gnutls_x509_privkey_export2(m_privkey, GNUTLS_X509_FMT_PEM, &out);
    if (ret < 0)
    {
        last_err = gnutls_strerror(ret);
        return "";
    }

    QByteArray qa;
    qa.append((const char*)out.data, out.size);
    gnutls_free(out.data);

    m_tmpfile.open();
    ret = m_tmpfile.write(qa);
    m_tmpfile.close();
    if (ret == -1)
    {
        return "";
    }
    return m_tmpfile.fileName();
}

QString Key::getUrl() const
{
    return m_url;
}
