/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QTemporaryFile>
extern "C" {
#include <gnutls/x509.h>
}

class QWidget;

class Key {
public:
    explicit Key(QWidget*);
    ~Key();

    Key(Key&&) noexcept = delete;
    Key& operator=(Key&&) noexcept = delete;
    Key(const Key&) = delete;
    Key& operator=(const Key&) = delete;

    /* functions return zero on success */
    int import_file(const QString&);
    int import_pem(QByteArray);
    void set(const gnutls_x509_privkey_t);
    int data_export(QByteArray&);
    QString exportTmpFile() const;
    QString getUrl() const;
    void clear();

    mutable QString last_err;
    bool IsImported = false;
private:
    int importKey(const QByteArray&);
    gnutls_x509_privkey_t m_privkey = nullptr;
    QString m_url;
    QWidget* m_window;
    mutable QTemporaryFile m_tmpfile;
};
