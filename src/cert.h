/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QTemporaryFile>
extern "C" {
#include <gnutls/x509.h>
}

class Cert {
public:
    Cert() = default;
    ~Cert();

    Cert(Cert&&) noexcept = delete;
    Cert& operator=(Cert&&) noexcept = delete;
    Cert(const Cert&) = delete;
    Cert& operator=(const Cert&) = delete;

    /* functions return zero on success */
    int import_file(const QString&);
    int import_pem(QByteArray);
    void set(gnutls_x509_crt_t);
    int data_export(QByteArray&);
    QString exportTmpFile() const;
    QString sha1_hash() const;
    void clear();

    mutable QString last_err;
    bool IsImported = false;
private:
    int import_cert(gnutls_datum_t*, bool);
    gnutls_x509_crt_t m_cert = nullptr;
    mutable QTemporaryFile m_tmpfile;
};
