/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "server_storage.h"
#include "cryptdata.h"
#include "common.h"
#include <QSettings>
#include <QMessageBox>
#include <QApplication>

StoredServer::StoredServer(QString p_profileName, QWidget* p_window)
    : m_profileName(std::move(p_profileName))
    , m_client(p_window)
{
    if (load() < 0) {
        QMessageBox::information(p_window,
            qApp->applicationName(),
            m_last_err.isEmpty() ? QObject::tr("Some profile information failed to load") : m_last_err);
    }
}

void StoredServer::clear_password()
{
    m_password.clear();
}

void StoredServer::clear_groupname()
{
    m_groupname.clear();
}

void StoredServer::clear_cert()
{
    m_client.m_cert.clear();
}

void StoredServer::clear_key()
{
    m_client.m_key.clear();
}

void StoredServer::clear_ca()
{
    m_ca_cert.clear();
}

void StoredServer::clear_server_hash()
{
    m_server_hash.clear();
    m_server_hash_algo = 0;
}

QString StoredServer::get_cert_file() const
{
    return m_client.m_cert.exportTmpFile();
}

QString StoredServer::get_key_file() const
{
    return m_client.m_key.exportTmpFile();
}

QString StoredServer::get_key_url() const
{       
    return m_client.m_key.getUrl();
}

QString StoredServer::get_ca_cert_file() const
{
    return m_ca_cert.exportTmpFile();
}

int StoredServer::set_ca_cert(const QString& filename)
{
    int ret = m_ca_cert.import_file(filename);
    m_last_err = m_ca_cert.last_err;
    return ret;
}

int StoredServer::set_client_cert(const QString& filename)
{
    int ret = m_client.import_cert(filename);
    m_last_err = m_client.last_err;

    if (ret) {
        ret = m_client.import_pfx(filename);
        m_last_err = m_client.last_err;
    }
    return ret;
}

int StoredServer::set_client_key(const QString& filename)
{
    int ret = m_client.import_key(filename);
    m_last_err = m_client.last_err;
    return ret;
}

QString StoredServer::get_server_hash() const
{
    if (m_server_hash_algo == 0) {
        return "";
    } else {
        return QString(gnutls_mac_get_name((gnutls_mac_algorithm_t)m_server_hash_algo)) + ":" + m_server_hash.toHex();
    }
}

int StoredServer::load()
{
    QSettings settings;
    settings.beginGroup("Profiles");
    if (not settings.childGroups().contains(m_profileName))
    {
        return 0;
    }
    settings.beginGroup(m_profileName);

    m_servername = settings.value("server").toString();
    if (m_servername.isEmpty()) {
        m_servername = m_profileName;
    }

    m_username = settings.value("username").toString();
    m_batch_mode = settings.value("batch", false).toBool();
    m_proxy = settings.value("proxy", false).toBool();
    m_disable_dtls = settings.value("disable-dtls", false).toBool();
    m_reconnect_timeout = settings.value("reconnect-timeout", 300).toInt();
    m_dtls_attempt_period = settings.value("dtls_attempt_period", 25).toInt();

    bool ret = false;
    int rval = 0;

    if (m_batch_mode) {
        m_groupname = settings.value("groupname").toString();
        ret = CryptData::decode(m_servername,
            settings.value("password").toByteArray(),
            m_password);
        if (not ret) {
            m_last_err = "decoding of password failed";
            rval = -1;
        }
    }

    QByteArray data;
    data = settings.value("ca-cert").toByteArray();
    if (data.isEmpty() or m_ca_cert.import_pem(std::move(data)) < 0) {
        m_last_err = m_ca_cert.last_err;
        rval = -1;
    }

    data = settings.value("client-cert").toByteArray();
    if (data.isEmpty() or m_client.m_cert.import_pem(std::move(data)) < 0) {
        m_last_err = m_client.m_cert.last_err;
        rval = -1;
    }

    QString str;
    ret = CryptData::decode(m_servername,
        settings.value("client-key").toByteArray(), str);
    if (not ret) {
        m_last_err = "decoding of client keyfailed";
        rval = -1;
    }

    if (is_url(str)) {
        m_client.m_key.import_file(str);
    } else {
        data = str.toLatin1();
        m_client.m_key.import_pem(std::move(data));
    }

    m_server_hash = settings.value("server-hash").toByteArray();
    m_server_hash_algo = settings.value("server-hash-algo").toInt();

    ret = CryptData::decode(m_servername,
        settings.value("token-str").toByteArray(),
        m_token_string);
    if (not ret) {
        m_last_err = "decoding of OTP token failed";
        rval = -1;
    }

    m_token_type = settings.value("token-type").toInt();

    m_protocol_id = settings.value("protocol-id", 0).toInt();
    m_protocol_name = settings.value("protocol-name").toString();

    settings.endGroup();
    settings.endGroup();
    return rval;
}

int StoredServer::save()
{
    QSettings settings;
    settings.beginGroup("Profiles/" + m_profileName);
    settings.setValue("server", m_servername);
    settings.setValue("batch", m_batch_mode);
    settings.setValue("proxy", m_proxy);
    settings.setValue("disable-dtls", m_disable_dtls);
    settings.setValue("reconnect-timeout", m_reconnect_timeout);
    settings.setValue("dtls_attempt_period", m_dtls_attempt_period);
    settings.setValue("username", m_username);

    if (m_batch_mode)
    {
        settings.setValue("password",
            CryptData::encode(m_servername, m_password));
        settings.setValue("groupname", m_groupname);
    }

    QByteArray data;
    m_ca_cert.data_export(data);
    settings.setValue("ca-cert", data);

    m_client.cert_export(data);
    settings.setValue("client-cert", data);

    m_client.key_export(data);
    QString str = QString::fromLatin1(data);
    settings.setValue("client-key", CryptData::encode(m_servername, str));

    settings.setValue("server-hash", m_server_hash);
    settings.setValue("server-hash-algo", m_server_hash_algo);

    settings.setValue("token-str",
        CryptData::encode(m_servername, m_token_string));
    settings.setValue("token-type", m_token_type);

    settings.setValue("protocol-id", m_protocol_id);
    settings.setValue("protocol-name", m_protocol_name);

    settings.endGroup();
    return 0;
}

const QString& StoredServer::get_username() const
{
    return m_username;
}

const QString& StoredServer::get_password() const
{
    return m_password;
}

const QString& StoredServer::get_groupname() const
{
    return m_groupname;
}

const QString& StoredServer::get_servername() const
{
    return m_servername;
}

const QString& StoredServer::getProfileName() const
{
    return m_profileName;
}

void StoredServer::setProfileName(const QString& p_newProfileName)
{
    m_profileName = p_newProfileName;
}

void StoredServer::set_username(const QString& username)
{
    m_username = username;
}

void StoredServer::set_password(const QString& password)
{
    m_password = password;
}

void StoredServer::set_groupname(const QString& groupname)
{
    m_groupname = groupname;
}

void StoredServer::set_servername(const QString& servername)
{
    m_servername = servername;
}

void StoredServer::setDisableDtls(bool v)
{
    m_disable_dtls = v;
}

bool StoredServer::isDtlsDisabled() const
{
    return m_disable_dtls;
}

QString StoredServer::get_client_cert_hash() const
{
    return m_client.m_cert.sha1_hash();
}

QString StoredServer::get_ca_cert_hash() const
{
    return m_ca_cert.sha1_hash();
}

void StoredServer::set_batch_mode(const bool mode)
{
    m_batch_mode = mode;
}

bool StoredServer::get_batch_mode() const
{
    return m_batch_mode;
}

bool StoredServer::get_proxy() const
{
    return m_proxy;
}

bool StoredServer::client_is_complete() const
{
    return m_client.is_complete();
}

void StoredServer::set_proxy(const bool t)
{
    m_proxy = t;
}

int StoredServer::get_reconnect_timeout() const
{
    return m_reconnect_timeout;
}

void StoredServer::set_reconnect_timeout(const int timeout)
{
    m_reconnect_timeout = timeout;
}

int StoredServer::get_dtls_reconnect_timeout() const
{
    return m_dtls_attempt_period;
}

void StoredServer::set_dtls_reconnect_timeout(const int timeout)
{
    m_dtls_attempt_period = timeout;
}

QString StoredServer::get_token_str()
{
    return m_token_string;
}

void StoredServer::set_token_str(const QString& str)
{
    m_token_string = str;
}

int StoredServer::get_token_type()
{
    return m_token_type;
}

void StoredServer::set_token_type(const int type)
{
    m_token_type = type;
}

int StoredServer::get_protocol_id() const
{
    return m_protocol_id;
}

void StoredServer::set_protocol_id(const int id)
{
    m_protocol_id = id;
}

QByteArray StoredServer::get_protocol_name() const
{
    return m_protocol_name.toLatin1();
}

void StoredServer::set_protocol_name(const QString name)
{
    m_protocol_name = name;
}

void StoredServer::set_server_hash(const unsigned algo, const QByteArray& hash)
{
    m_server_hash_algo = algo;
    m_server_hash = hash;
}

unsigned StoredServer::get_server_hash(QByteArray& hash) const
{
    hash = m_server_hash;
    return m_server_hash_algo;
}
