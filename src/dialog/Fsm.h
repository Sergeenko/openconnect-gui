#pragma once

#include <QStateMachine>
#include <QState>
#include <QScopedPointer>
#include "EventTransitions.h"

class MainWindow;

class ConnectionFsm final
{
public:
    explicit ConnectionFsm(MainWindow&);
    void init();
private:
    MainWindow& m_mw;
    QStateMachine m_stateMachine;
    QState m_noProfiles;
    QState m_connectionReady;
    QScopedPointer<ProfileListTransition> m_ToConnReady;
    QScopedPointer<ProfileListTransition> m_ToNoProfiles;
};

class WindowFsm final
{
public:
    explicit WindowFsm(MainWindow&);
    void init();
private:
    MainWindow& m_mw;
    QStateMachine m_stateMachine;
    QState m_normalWindow;
    QState m_minimizedWindow;
    WindowEventTransition m_minimize;
    WindowEventTransition m_restore;
};

