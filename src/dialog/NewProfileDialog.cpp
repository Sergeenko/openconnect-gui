#include "NewProfileDialog.h"
#include "server_storage.h"

#include <QPushButton>
#include <QSettings>
#include <QUrl>

NewProfileDialog::NewProfileDialog(QWidget* parent)
    : QDialog(parent)
{
    m_ui.setupUi(this);
    m_ui.protocolComboBox->setModel(&m_vpnProtocols);

    m_ui.buttonBox->button(QDialogButtonBox::SaveAll)->setText(tr("Save and Connect"));
    m_ui.buttonBox->button(QDialogButtonBox::SaveAll)->setDefault(true);

    m_ui.buttonBox->button(QDialogButtonBox::Save)->setEnabled(false);
    m_ui.buttonBox->button(QDialogButtonBox::SaveAll)->setEnabled(false);
}

QString NewProfileDialog::getNewProfileName() const
{
    return m_ui.lineEditName->text();
}

void NewProfileDialog::changeEvent(QEvent* e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        m_ui.retranslateUi(this);
        break;
    default:
        break;
    }
}

void NewProfileDialog::on_checkBoxCustomize_toggled(bool checked)
{
    if (not checked)
    {
        QUrl url(m_ui.lineEditGateway->text());
        if (url.isValid())
        {
            m_ui.lineEditName->setText(url.host());
        }

        m_ui.lineEditGateway->setFocus();
    }
    else
    {
        m_ui.lineEditName->setFocus();
    }
}

void NewProfileDialog::on_lineEditName_textChanged()
{
    updateButtons();
}

void NewProfileDialog::on_lineEditGateway_textChanged(const QString& text)
{
    QUrl url(text, QUrl::StrictMode);
    if (not m_ui.checkBoxCustomize->isChecked() and (url.isValid() or text.isEmpty()))
    {
        m_ui.lineEditName->setText(url.host());
    }

    updateButtons();
}

void NewProfileDialog::updateButtons()
{
    bool enableButtons{ false };
    if (not m_ui.lineEditGateway->text().isEmpty())
    {
        QSettings settings;
        settings.beginGroup("Profiles");
        if (not settings.childGroups().contains(m_ui.lineEditName->text()))
        {
            enableButtons = true;
        }
    }

    m_ui.buttonBox->button(QDialogButtonBox::Save)->setEnabled(enableButtons);
    m_ui.buttonBox->button(QDialogButtonBox::SaveAll)->setEnabled(enableButtons);
}

void NewProfileDialog::on_buttonBox_clicked(QAbstractButton* button)
{
    if (m_ui.buttonBox->standardButton(button) == QDialogButtonBox::SaveAll)
    {
        emit connect();
    }
}

void NewProfileDialog::on_buttonBox_accepted()
{
    StoredServer ss(m_ui.lineEditName->text(), this);

    if (m_ui.lineEditName->text().isEmpty())
    {
        QSettings settings;
        settings.beginGroup("Profiles");
        QString l_newProfileName(m_ui.lineEditGateway->text());
        while (settings.childGroups().contains(l_newProfileName))
        {
            l_newProfileName += "Copy";
        }
        ss.setProfileName(l_newProfileName);
    }

    ss.set_servername(m_ui.lineEditGateway->text());
    ss.set_protocol_id(m_ui.protocolComboBox->currentIndex());
    ss.set_protocol_name(m_ui.protocolComboBox->currentData(Qt::UserRole + 1).toString());
    ss.save();

    accept();
}
