/*
 * Copyright (C) 2014, 2015 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "NewProfileDialog.h"
#include "editdialog.h"
#include "logdialog.h"
#include "openconnect-gui.h"
#include "timestamp.h"
#include "vpninfo.h"
#include "logger.h"
#include <spdlog/spdlog.h>
#include <QCloseEvent>
#include <QDesktopServices>
#include <QDialog>
#include <QFileSelector>
#include <QLineEdit>
#include <QMessageBox>
#include <QSettings>
#include <QtConcurrent/QtConcurrentRun>
#include <QException>
extern "C"
{
#include <openconnect.h>
}
#ifdef _WIN32
#define pipe_write(x, y, z) send(x, y, z, 0)
#define net_errno WSAGetLastError()
#define ms_sleep Sleep
#else
#define pipe_write(x, y, z) write(x, y, z)
#include <errno.h>
#include <cerrno>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <cmath>
#include <cstdarg>
#include <cstdio>
#define ms_sleep(x) usleep(1000 * x)
#define INVALID_SOCKET -1
#define closesocket close
#define net_errno errno
#endif

#define OFF_ICON QPixmap(QString::fromLatin1(":/images/traffic_light_red.png"))
#define ON_ICON QPixmap(QString::fromLatin1(":/images/traffic_light_green.png"))
#define CONNECTING_ICON QPixmap(QString::fromLatin1(":/images/traffic_light_yellow.png"))
#define CONNECTING_ICON2 QPixmap(QString::fromLatin1(":/images/traffic_light_off.png"))

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , cmd_fd(INVALID_SOCKET)
    , timer(this)
    , blink_timer(this)
    , m_serverProfilesMenu(this)
    , m_connectionFsm(*this)
    , m_windowFsm(*this)
{
    ui.setupUi(this);

    connect(ui.viewLogButton, &QPushButton::clicked,
        this, &MainWindow::createLogDialog);

    connect(ui.actionQuit, &QAction::triggered,
        [=]() {
            if (m_trayDisconnectAction->isEnabled()) {
                connect(this, &MainWindow::readyToShutdown,
                    qApp, &QApplication::quit);
                on_disconnectClicked();
            } else {
                qApp->quit();
            }
        });

    connect(&blink_timer, &QTimer::timeout,
        this, &MainWindow::blink_ui,
        Qt::QueuedConnection);
    connect(&timer, &QTimer::timeout,
        this, &MainWindow::request_update_stats,
        Qt::QueuedConnection);
    connect(this, &MainWindow::vpn_status_changed_sig,
        this, &MainWindow::changeStatus,
        Qt::QueuedConnection);
    connect(ui.connectionButton, &QPushButton::clicked,
        this, &MainWindow::on_connectClicked,
        Qt::QueuedConnection);
    connect(this, &MainWindow::stats_changed_sig,
        this, &MainWindow::statsChanged,
        Qt::QueuedConnection);

    ui.iconLabel->setPixmap(OFF_ICON);

    if (QSystemTrayIcon::isSystemTrayAvailable())
    {
        createTrayIcon();

        connect(m_trayIcon, &QSystemTrayIcon::activated,
            this, &MainWindow::iconActivated);

        QFileSelector selector;
        QIcon icon(selector.select(":/images/network-disconnected.png"));
        icon.setIsMask(true);
        m_trayIcon->setIcon(icon);
        m_trayIcon->show();
    }

    m_serverProfilesMenu.addAction(ui.actionNewProfile);
    m_serverProfilesMenu.addAction(ui.actionNewProfileAdvanced);
    m_serverProfilesMenu.addAction(ui.actionEditSelectedProfile);
    m_serverProfilesMenu.addAction(ui.actionRemoveSelectedProfile);
    ui.serverListControl->setMenu(&m_serverProfilesMenu);

    readSettings();
    m_windowFsm.init();
    m_connectionFsm.init();
}

void MainWindow::Disconnect()
{
    if (cmd_fd not_eq INVALID_SOCKET)
    {
        vpn_status_changed(STATUS_DISCONNECTING);
        const char cmd = OC_CMD_CANCEL;
        int ret = pipe_write(cmd_fd, &cmd, 1);
        if (ret < 0) {
            Logger::instance().addMessage("Disconnect: IPC error: " + QString::number(net_errno));
        }
        cmd_fd = INVALID_SOCKET;
        ms_sleep(200);
    }
    vpn_status_changed(STATUS_DISCONNECTED);
}

MainWindow::~MainWindow()
{
    int counter = 10;
    if (timer.isActive()) {
        timer.stop();
    }

    if (futureWatcher.isRunning()) {
        Disconnect();
    }
    while (futureWatcher.isRunning() and counter > 0) {
        ms_sleep(200);
        --counter;
    }

    writeSettings();
}

void MainWindow::vpn_status_changed(int connected)
{
    emit vpn_status_changed_sig(connected);
}

void MainWindow::vpn_status_changed(int connected, QString& p_dns, QString& p_ip, QString& p_ip6, QString& p_cstp_cipher, QString& p_dtls_cipher)
{
    dns = p_dns;
    ip = p_ip;
    ip6 = p_ip6;
    dtls_cipher = p_dtls_cipher;
    cstp_cipher = p_cstp_cipher;

    emit vpn_status_changed_sig(connected);
}

QString MainWindow::normalize_byte_size(uint64_t bytes)
{
    const unsigned unit = 1024; // TODO: add support for SI units? (optional)
    if (bytes < unit)
    {
        return QString("%1 B").arg(QString::number(bytes));
    }
    const int exp = static_cast<int>(std::log(bytes) / std::log(unit));
    static const char suffixChar[] = "KMGTPE";
    return QString("%1 %2B").arg(QString::number(bytes / std::pow(unit, exp), 'f', 3)).arg(suffixChar[exp - 1]);
}

void MainWindow::statsChanged(QString tx, QString rx, QString dtls)
{
    ui.downloadLabel->setText(rx);
    ui.uploadLabel->setText(tx);
    ui.cipherDTLSLabel->setText(dtls);
}

void MainWindow::updateStats(const struct oc_stats* stats, QString dtls)
{
    emit stats_changed_sig(
        normalize_byte_size(stats->tx_bytes),
        normalize_byte_size(stats->rx_bytes),
        dtls);
}

void MainWindow::reload_settings()
{
    ui.serverList->clear();
    m_trayIconMenuConnections->clear();

    QSettings settings;
    settings.beginGroup("Profiles");
    for (const auto& l_profile : settings.childGroups())
    {
        ui.serverList->addItem(l_profile);
        QAction* act = m_trayIconMenuConnections->addAction(l_profile);
        connect(act, &QAction::triggered, [act, this]()
        {
            int idx = ui.serverList->findText(act->text());
            if (idx not_eq -1)
            {
                ui.serverList->setCurrentIndex(idx);
                on_connectClicked();
            }
        });
    }
}

void MainWindow::blink_ui()
{
    static unsigned t = 1;

    if (t % 2 == 0) {
        ui.iconLabel->setPixmap(CONNECTING_ICON);
    } else {
        ui.iconLabel->setPixmap(CONNECTING_ICON2);
    }
    ++t;
}

void MainWindow::changeStatus(int val)
{
    if (val == STATUS_CONNECTED) {

        blink_timer.stop();

        ui.serverList->setEnabled(false);

        m_trayIconMenuConnections->setEnabled(false);
        m_trayDisconnectAction->setEnabled(true);

        ui.iconLabel->setPixmap(ON_ICON);
        ui.connectionButton->setIcon(QIcon(":/images/process-stop.png"));
        ui.connectionButton->setText(tr("Disconnect"));

        QFileSelector selector;
        QIcon icon(selector.select(QStringLiteral(":/images/network-connected.png")));
        icon.setIsMask(true);
        m_trayIcon->setIcon(icon);

        ui.ipV4Label->setText(ip);
        ui.ipV6Label->setText(ip6);
        ui.dnsLabel->setText(dns);
        ui.cipherCSTPLabel->setText(cstp_cipher);
        ui.cipherDTLSLabel->setText(dtls_cipher);

        const int l_timeout = 10000;
        timer.start(l_timeout);

        QSettings settings;
        if (settings.value("System/Settings/minimizeOnConnect").toBool()) {
            if (m_trayIcon) {
                hide();
                m_trayIcon->showMessage(QLatin1String("Connected"), QLatin1String("You are connected to ") + ui.serverList->currentText(),
                    QSystemTrayIcon::Information,
                    10000);
            } else {
                setWindowState(Qt::WindowMinimized);
            }
        }
    } else if (val == STATUS_CONNECTING) {

        if (m_trayIcon) {
            QFileSelector selector;
            QIcon icon(selector.select(QStringLiteral(":/images/network-disconnected.png")));
            icon.setIsMask(true);
            m_trayIcon->setIcon(icon);
        }

        ui.serverList->setEnabled(false);

        m_trayIconMenuConnections->setEnabled(false);
        m_trayDisconnectAction->setEnabled(true);

        ui.iconLabel->setPixmap(CONNECTING_ICON);
        ui.connectionButton->setIcon(QIcon(":/images/process-stop.png"));
        ui.connectionButton->setText(tr("Cancel"));
        blink_timer.start(1500);

        disconnect(ui.connectionButton, &QPushButton::clicked,
            this, &MainWindow::on_connectClicked);
        connect(ui.connectionButton, &QPushButton::clicked,
            this, &MainWindow::on_disconnectClicked,
            Qt::QueuedConnection);
    } else if (val == STATUS_DISCONNECTED) {
        blink_timer.stop();
        if (timer.isActive()) {
            timer.stop();
        }
        cmd_fd = INVALID_SOCKET;

        ui.ipV4Label->clear();
        ui.ipV6Label->clear();
        ui.dnsLabel->clear();
        ui.uploadLabel->clear();
        ui.downloadLabel->clear();
        ui.cipherCSTPLabel->clear();
        ui.cipherDTLSLabel->clear();
        Logger::instance().addMessage("Disconnected");

        ui.serverList->setEnabled(true);

        m_trayIconMenuConnections->setEnabled(true);
        m_trayDisconnectAction->setEnabled(false);

        ui.iconLabel->setPixmap(OFF_ICON);
        ui.connectionButton->setEnabled(true);
        ui.connectionButton->setIcon(QIcon(":/images/network-wired.png"));
        ui.connectionButton->setText(tr("Connect"));

        if (m_trayIcon) {
            QFileSelector selector;
            QIcon icon(selector.select(QStringLiteral(":/images/network-disconnected.png")));
            icon.setIsMask(true);
            m_trayIcon->setIcon(icon);

            if (isHidden())
                m_trayIcon->showMessage(QLatin1String("Disconnected"), QLatin1String("You were disconnected from the VPN"),
                    QSystemTrayIcon::Warning,
                    10000);
        }
        disconnect(ui.connectionButton, &QPushButton::clicked,
            this, &MainWindow::on_disconnectClicked);
        connect(ui.connectionButton, &QPushButton::clicked,
            this, &MainWindow::on_connectClicked,
            Qt::QueuedConnection);

        emit readyToShutdown();
    } else if (val == STATUS_DISCONNECTING) {
        ui.iconLabel->setPixmap(CONNECTING_ICON);
        ui.connectionButton->setIcon(QIcon(":/images/process-stop.png"));
        ui.connectionButton->setEnabled(false);
        blink_timer.start(1500);
    } else {
        qDebug() << "TODO: was is das?";
    }
}

static int tryConnect(const VpnInfo& p_vpninfo)
{
    int connect_status = p_vpninfo.connect();
    if (connect_status == 0)
    {
        connect_status = p_vpninfo.dtls_connect();
    }
    return connect_status;
}

void MainWindow::mainLoop(QString p_profileName)
{
    vpn_status_changed(STATUS_CONNECTING);
    VpnInfo l_vpninfo(QStringLiteral("Open AnyConnect VPN Agent"), std::move(p_profileName), *this);
    try
    {
        l_vpninfo.parse_url();
    }
    catch (QException& ex)
    {
        QMessageBox::information(this,
            qApp->applicationName(),
            tr("There was an issue initializing the VPN ") + "(" + ex.what() + ")");
        return;
    }

    cmd_fd = l_vpninfo.get_cmd_fd();

    if (tryConnect(l_vpninfo) not_eq 0)
    {
        if (l_vpninfo.m_ss.get_batch_mode() and not l_vpninfo.m_ss.get_password().isEmpty())
        {
            /* authentication failed in batch mode? clear the password and retry */
            QString oldpass = l_vpninfo.m_ss.get_password();
            QString oldgroup = l_vpninfo.m_ss.get_groupname();
            l_vpninfo.m_ss.clear_password();
            l_vpninfo.m_ss.clear_groupname();
            Logger::instance().addMessage("Authentication failed in batch mode, retrying with batch mode disabled");
            l_vpninfo.reset_vpn();

            if (tryConnect(l_vpninfo) not_eq 0)
            {
                /* if we didn't manage to connect on a retry, the failure reason
                 * may not have been a changed password, reset it */
                l_vpninfo.m_ss.set_password(oldpass);
                l_vpninfo.m_ss.set_groupname(oldgroup);

                Logger::instance().addMessage(std::move(l_vpninfo.last_err));
                vpn_status_changed(STATUS_DISCONNECTED);
                return;
            }
        }
        else
        {
            Logger::instance().addMessage(std::move(l_vpninfo.last_err));
            vpn_status_changed(STATUS_DISCONNECTED);
            return;
        }
    }

    QString ip, ip6, dns, cstp, dtls;
    l_vpninfo.get_info(dns, ip, ip6);
    l_vpninfo.get_cipher_info(cstp, dtls);
    vpn_status_changed(STATUS_CONNECTED, dns, ip, ip6, cstp, dtls);
    l_vpninfo.m_ss.save();
    l_vpninfo.openconnectLoop();
}

void MainWindow::on_disconnectClicked()
{
    if (timer.isActive()) {
        timer.stop();
    }
    Logger::instance().addMessage("Disconnecting...");
    Disconnect();
}

void MainWindow::on_connectClicked()
{
    if (cmd_fd not_eq INVALID_SOCKET) {
        QMessageBox::information(this,
            qApp->applicationName(),
            tr("A previous VPN instance is still running (socket is active)"));
        return;
    }

    if (futureWatcher.isRunning()) {
        QMessageBox::information(this,
            qApp->applicationName(),
            tr("A previous VPN instance is still running"));
        return;
    }

    if (ui.serverList->currentText().isEmpty()) {
        QMessageBox::information(this,
            qApp->applicationName(),
            tr("You need to specify a gateway. e.g. vpn.example.com:443"));
        return;
    }

    futureWatcher.setFuture(QtConcurrent::run(&MainWindow::mainLoop, this, ui.serverList->currentText()));
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    if (m_trayIcon and m_trayIcon->isVisible() and ui.actionMinimizeTheApplicationInsteadOfClosing->isChecked()) {
        showMinimized();
        event->ignore();
    } else {
        event->accept();

        if (m_trayDisconnectAction->isEnabled()) {
            connect(this, &MainWindow::readyToShutdown,
                qApp, &QApplication::quit);
            on_disconnectClicked();
        } else {
            qApp->quit();
        }
    }
    QMainWindow::closeEvent(event);
}

void MainWindow::request_update_stats()
{
    char cmd = OC_CMD_STATS;
    if (cmd_fd not_eq INVALID_SOCKET) {
        int ret = pipe_write(cmd_fd, &cmd, 1);
        if (ret < 0) {
            Logger::instance().addMessage("update_stats: IPC error: " + QString::number(net_errno));
            if (timer.isActive())
                timer.stop();
        }
    } else {
        Logger::instance().addMessage("update_stats: invalid socket");
        if (timer.isActive())
            timer.stop();
    }
}

void MainWindow::readSettings()
{
    QSettings settings;
    settings.beginGroup("System/MainWindow");
    resize(settings.value("size").toSize());
    if (settings.contains("pos")) {
        move(settings.value("pos").toPoint());
    }
    settings.endGroup();

    settings.beginGroup("System/Settings");
    ui.actionMinimizeToTheNotificationArea->setChecked(settings.value("minimizeToTheNotificationArea", true).toBool());
    ui.actionMinimizeTheApplicationInsteadOfClosing->setChecked(settings.value("minimizeTheApplicationInsteadOfClosing", true).toBool());
    ui.actionMinimizeOnConnect->setChecked(settings.value("minimizeOnConnect", false).toBool());
    ui.actionStartMinimized->setChecked(settings.value("startMinimized", false).toBool());
    settings.endGroup();
}

void MainWindow::writeSettings()
{
    QSettings settings;
    settings.beginGroup("System/MainWindow");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();

    settings.beginGroup("System/Settings");
    settings.setValue("minimizeToTheNotificationArea", ui.actionMinimizeToTheNotificationArea->isChecked());
    settings.setValue("minimizeTheApplicationInsteadOfClosing", ui.actionMinimizeTheApplicationInsteadOfClosing->isChecked());
    settings.setValue("minimizeOnConnect", ui.actionMinimizeOnConnect->isChecked());
    settings.setValue("startMinimized", ui.actionStartMinimized->isChecked());
    settings.endGroup();

    settings.setValue("Profiles/currentIndex", ui.serverList->currentIndex());
}

void MainWindow::createLogDialog()
{
    auto dialog{ new LogDialog() };

    disconnect(ui.viewLogButton, &QPushButton::clicked,
        this, &MainWindow::createLogDialog);

    connect(ui.viewLogButton, &QPushButton::clicked,
        dialog, &QDialog::show);
    connect(ui.viewLogButton, &QPushButton::clicked,
        dialog, &QDialog::raise);
    connect(ui.viewLogButton, &QPushButton::clicked,
        dialog, &QDialog::activateWindow);

    connect(dialog, &QDialog::finished,
        [this]() {
            connect(ui.viewLogButton, &QPushButton::clicked,
                this, &MainWindow::createLogDialog);
        });
    connect(dialog, &QDialog::finished,
        dialog, &QDialog::deleteLater);

    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void MainWindow::createTrayIcon()
{
    m_trayIconMenu = new QMenu(this);

    m_trayIconMenuConnections = new QMenu(this);
    m_trayIconMenu->addMenu(m_trayIconMenuConnections);
    m_trayDisconnectAction = new QAction(tr("Disconnect"), this);
    m_trayIconMenu->addAction(m_trayDisconnectAction);
    connect(m_trayDisconnectAction, &QAction::triggered,
        this, &MainWindow::on_disconnectClicked);

    m_trayIconMenu->addSeparator();
    m_trayIconMenu->addAction(ui.actionLogWindow);
    m_trayIconMenu->addSeparator();
    m_trayIconMenu->addAction(ui.actionMinimize);
    m_trayIconMenu->addAction(ui.actionRestore);
    m_trayIconMenu->addSeparator();
    m_trayIconMenu->addAction(ui.actionQuit);

    m_trayIcon = new QSystemTrayIcon(this);
    m_trayIcon->setContextMenu(m_trayIconMenu);
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
    case QSystemTrayIcon::MiddleClick:
#ifdef Q_OS_WIN
        if (isMinimized()) {
            showNormal();
        } else {
            showMinimized();
        }
#endif
        break;
    default:
        break;
    }
}

void MainWindow::on_actionNewProfile_triggered()
{
    NewProfileDialog dialog(this);
    connect(&dialog, &NewProfileDialog::connect,
        this, &MainWindow::on_connectClicked,
        Qt::QueuedConnection);
    if (dialog.exec() not_eq QDialog::Accepted)
    {
        return;
    }

    reload_settings();
    ui.serverList->setCurrentText(dialog.getNewProfileName());
}

void MainWindow::on_actionNewProfileAdvanced_triggered()
{
    EditDialog dialog("", this);
    if (dialog.exec() not_eq QDialog::Accepted)
    {
        return;
    }

    reload_settings();
    ui.serverList->setCurrentText(dialog.getEditedProfileName());
}

void MainWindow::on_actionEditSelectedProfile_triggered()
{
    EditDialog dialog(ui.serverList->currentText(), this);
    if (dialog.exec() not_eq QDialog::Accepted)
    {
        return;
    }

    int idx = ui.serverList->currentIndex();
    reload_settings();
    // TODO: may be signal/slot?
    if (idx < ui.serverList->maxVisibleItems() and idx >= 0) {
        ui.serverList->setCurrentIndex(idx);
    }
    else
    {
        ui.serverList->setCurrentIndex(0);
    }
}

void MainWindow::on_actionRemoveSelectedProfile_triggered()
{
    QMessageBox mbox;
    mbox.setText(tr("Are you sure you want to remove '%1' host?").arg(ui.serverList->currentText()));
    mbox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
    mbox.setDefaultButton(QMessageBox::Cancel);
    if (mbox.exec() == QMessageBox::Ok)
    {
        QSettings settings;
        settings.beginGroup("Profiles");
        if (settings.childGroups().contains(ui.serverList->currentText()))
        {
            settings.remove(ui.serverList->currentText());
        }

        reload_settings();
    }
}

void MainWindow::on_actionAbout_triggered()
{
    QString txt = QLatin1String("<h2>") + QLatin1String(appDescriptionLong) + QLatin1String("</h2>");
    txt += tr("Version <i>%1</i> (%2)").arg(appVersion, QSysInfo::buildCpuArchitecture());
    txt += tr("<br><br>Build on ") + QLatin1String("<i>") + QLatin1String(appBuildOn) + QLatin1String("</i>");
    txt += tr("<br>Based on");
    txt += tr("<br>- <a href=\"https://www.infradead.org/openconnect\">OpenConnect</a> ") + QLatin1String(openconnect_get_version());
    txt += tr("<br>- <a href=\"https://www.gnutls.org\">GnuTLS</a> v") + QLatin1String(gnutls_check_version(nullptr));
    txt += tr("<br>- <a href=\"https://github.com/gabime/spdlog\">spdlog</a> v%1.%2.%3").arg(
        QString::number(SPDLOG_VER_MAJOR),
        QString::number(SPDLOG_VER_MINOR),
        QString::number(SPDLOG_VER_PATCH));
    txt += tr("<br>- <a href=\"https://www.qt.io\">Qt</a> v%1").arg(QT_VERSION_STR);
    txt += tr("<br><br>%1<br>").arg(appCopyright);
    txt += tr("<br><i>%1</i> comes with ABSOLUTELY NO WARRANTY. This is free software, "
              "and you are welcome to redistribute it under the conditions "
              "of the GNU General Public License version 2.")
               .arg(appDescriptionLong);

    QMessageBox::about(this, "", txt);
}

void MainWindow::on_actionAboutQt_triggered()
{
    qApp->aboutQt();
}

void MainWindow::on_actionWebSite_triggered()
{
    QDesktopServices::openUrl(QUrl("https://openconnect.github.io/openconnect-gui"));
}
