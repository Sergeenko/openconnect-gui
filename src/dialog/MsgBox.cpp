/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MsgBox.h"
#include <QMessageBox>
#include <QApplication>

MsgBox::MsgBox(QWidget& p_w, QString p_t1, QString p_t2, QString p_details)
    : m_eventResult(false)
    , m_w(p_w)
    , m_t1(p_t1)
    , m_t2(p_t2)
    , m_details(p_details)
{
    moveToThread(QApplication::instance()->thread());
}

void MsgBox::popUpMsgBox()
{
    QCoreApplication::postEvent(this, new QEvent(QEvent::User));
}

bool MsgBox::event(QEvent* ev)
{
    if (ev->type() == QEvent::User)
    {
        {
            QMutexLocker lock(&m_mutex);
            QMessageBox msgBox(&m_w);
            msgBox.setText(m_t1);
            msgBox.setInformativeText(m_t2);
            msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Cancel);

            if (not m_details.isEmpty())
                msgBox.setDetailedText(m_details);

            m_eventResult = msgBox.exec() == QMessageBox::Ok;
        }
        m_cv.wakeOne();
        return true;
    }
    return false;
}

bool MsgBox::isOkClicked()
{
    QMutexLocker lock(&m_mutex);
    popUpMsgBox();
    m_cv.wait(&m_mutex);
    return m_eventResult;
}
