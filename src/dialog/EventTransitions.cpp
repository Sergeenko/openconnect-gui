#include "EventTransitions.h"

ProfileListTransition::ProfileListTransition(const QComboBox* cb, bool hasServers)
    : QSignalTransition(cb, SIGNAL(currentIndexChanged(int)))
    , hasServers(hasServers)
{}

bool ProfileListTransition::eventTest(QEvent* e)
{
    if (not QSignalTransition::eventTest(e)) {
        return false;
    }
    QStateMachine::SignalEvent* l_se = static_cast<QStateMachine::SignalEvent*>(e);
    bool isEmpty = l_se->arguments().at(0).toInt() == -1;
    return (hasServers ? not isEmpty : isEmpty);
}


 WindowEventTransition::WindowEventTransition(QMainWindow& mw, Qt::WindowState state)
     : QEventTransition(&mw, QEvent::WindowStateChange)
     , m_mw(mw)
     , m_state(state)
 {}

bool WindowEventTransition::eventTest(QEvent* e)
{
    if (not QEventTransition::eventTest(e)) {
        return false;
    }
    QStateMachine::WrappedEvent* l_we = static_cast<QStateMachine::WrappedEvent*>(e);
    if (l_we->event()->type() == QEvent::WindowStateChange) {
        return (m_mw.windowState() == m_state);
    }
    return false;
}
