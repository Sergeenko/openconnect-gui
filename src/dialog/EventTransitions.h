#pragma once

#include <QEventTransition>
#include <QSignalTransition>
#include <QComboBox>
#include <QStateMachine>
#include <QMainWindow>

class ProfileListTransition : public QSignalTransition {
public:
    ProfileListTransition(const QComboBox* cb, bool hasServers);
protected:
    bool eventTest(QEvent* e) override;
private:
    bool hasServers;
};

class WindowEventTransition : public QEventTransition {
public:
    WindowEventTransition(QMainWindow& mw, Qt::WindowState state);
protected:
    bool eventTest(QEvent* e) override;
private:
    QMainWindow& m_mw;
    Qt::WindowState m_state;
};
