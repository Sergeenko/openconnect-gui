/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QDialog>
#include <QTimer>
#include "logger.h"
#include "ui_logdialog.h"

class LogDialog : public QDialog {
    Q_OBJECT
public:
    explicit LogDialog(QWidget* parent = nullptr);
    ~LogDialog();

public slots:
    void append(const Logger::Message& message);

private slots:
    void on_pushButtonClear_clicked();
    void on_pushButtonSelectAll_clicked();
    void on_pushButtonCopy_clicked();
    void onItemSelectionChanged();
    void on_checkBox_autoScroll_toggled(bool checked);
    void on_LogDialog_rejected();
private:
    void loadSettings();
    void saveSettings();

    Ui::LogDialog ui;
    QTimer m_timer;
};
