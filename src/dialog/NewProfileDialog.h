#pragma once

#include <QDialog>
#include "ui_NewProfileDialog.h"
#include "VpnProtocolModel.h"

class QAbstractButton;

class NewProfileDialog : public QDialog {
    Q_OBJECT
public:
    explicit NewProfileDialog(QWidget* parent = 0);
    QString getNewProfileName() const;
signals:
    void connect();
protected:
    void changeEvent(QEvent* e);
private slots:
    void on_checkBoxCustomize_toggled(bool checked);
    void on_lineEditName_textChanged();
    void on_lineEditGateway_textChanged(const QString&);
    void on_buttonBox_clicked(QAbstractButton* button);
    void on_buttonBox_accepted();
private:
    void updateButtons();
    Ui::NewProfileDialog m_ui;
    VpnProtocolModel m_vpnProtocols;
};
