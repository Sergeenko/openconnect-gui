#include "Fsm.h"
#include "mainwindow.h"
#include <QSettings>

ConnectionFsm::ConnectionFsm(MainWindow& p_mw)
    : m_mw(p_mw)
    , m_stateMachine(&p_mw)
{}

void ConnectionFsm::init()
{
    m_noProfiles.assignProperty(m_mw.ui.connectionButton, "enabled", false);
    m_noProfiles.assignProperty(m_mw.ui.serverList, "enabled", false);
    m_noProfiles.assignProperty(m_mw.ui.actionEditSelectedProfile, "enabled", false);
    m_noProfiles.assignProperty(m_mw.ui.actionRemoveSelectedProfile, "enabled", false);
    if (m_mw.m_trayIconMenuConnections)
    {
        m_noProfiles.assignProperty(m_mw.m_trayIconMenuConnections, "title", "No profiles to connect");
        m_noProfiles.assignProperty(m_mw.m_trayIconMenuConnections, "enabled", false);
        m_noProfiles.assignProperty(m_mw.m_trayDisconnectAction, "enabled", false);
    }
    m_stateMachine.addState(&m_noProfiles);

    m_connectionReady.assignProperty(m_mw.ui.connectionButton, "enabled", true);
    m_connectionReady.assignProperty(m_mw.ui.serverList, "enabled", true);
    m_connectionReady.assignProperty(m_mw.ui.actionEditSelectedProfile, "enabled", true);
    m_connectionReady.assignProperty(m_mw.ui.actionRemoveSelectedProfile, "enabled", true);
    if (m_mw.m_trayIconMenuConnections)
    {
        m_connectionReady.assignProperty(m_mw.m_trayIconMenuConnections, "title", "Connect to");
        m_connectionReady.assignProperty(m_mw.m_trayIconMenuConnections, "enabled", true);
    }
    m_stateMachine.addState(&m_connectionReady);

    m_ToConnReady.reset(new ProfileListTransition(m_mw.ui.serverList, true));
    m_ToConnReady->setTargetState(&m_connectionReady);
    m_noProfiles.addTransition(m_ToConnReady.get());

    m_ToNoProfiles.reset(new ProfileListTransition(m_mw.ui.serverList, false));
    m_ToNoProfiles->setTargetState(&m_noProfiles);
    m_connectionReady.addTransition(m_ToNoProfiles.get());

    m_stateMachine.setInitialState(&m_noProfiles);
    m_stateMachine.start();
    m_mw.connect(&m_stateMachine, &QStateMachine::started, [=]() {
        // LCA: find better way to load/fill combobox...
        m_mw.reload_settings();

//TODO: Add autoconnect option per profile (for single only)
//        if (not profileName.isEmpty()) {
//            // TODO: better place when refactor SM...
//            const int profileIndex = ui.serverList->findText(profileName);
//            if (profileIndex not_eq -1) {
//                ui.serverList->setCurrentIndex(profileIndex);
//                on_connectClicked();
//                return;
//            } else {
//                QMessageBox::warning(this,
//                    tr("Connection failed"),
//                    tr("Selected VPN profile '<b>%1</b>' does not exists.").arg(profileName));
//            }
//        }

        QSettings settings;
        const int currentIndex = settings.value("Profiles/currentIndex", -1).toInt();
        if (currentIndex not_eq -1 and currentIndex < m_mw.ui.serverList->count())
        {
            m_mw.ui.serverList->setCurrentIndex(currentIndex);
        }
    });
}

WindowFsm::WindowFsm(MainWindow& p_mw)
    : m_mw(p_mw)
    , m_stateMachine(&p_mw)
    , m_minimize(p_mw, Qt::WindowMinimized)
    , m_restore(p_mw, Qt::WindowNoState)
{}

void WindowFsm::init()
{
    m_stateMachine.addState(&m_normalWindow);
    m_normalWindow.assignProperty(m_mw.ui.actionRestore, "enabled", false);
    m_normalWindow.assignProperty(m_mw.ui.actionMinimize, "enabled", true);

    m_stateMachine.addState(&m_minimizedWindow);
    m_mw.connect(&m_minimizedWindow, &QState::entered, [=]() {
        m_mw.showMinimized();
        if (m_mw.ui.actionMinimizeToTheNotificationArea->isChecked())
        {
            QTimer::singleShot(10, &m_mw, SLOT(hide()));
        }
    });
    m_mw.connect(&m_minimizedWindow, &QState::exited, [=]() {
        m_mw.showNormal();
        if (m_mw.ui.actionMinimizeToTheNotificationArea->isChecked())
        {
            m_mw.show();
            m_mw.raise();
            m_mw.activateWindow();
        }
    });
    m_minimizedWindow.assignProperty(m_mw.ui.actionRestore, "enabled", true);
    m_minimizedWindow.assignProperty(m_mw.ui.actionMinimize, "enabled", false);

    if (m_mw.ui.actionStartMinimized->isChecked())
    {
        m_stateMachine.setInitialState(&m_minimizedWindow);
    }
    else
    {
        m_stateMachine.setInitialState(&m_normalWindow);
    }

    m_minimize.setTargetState(&m_minimizedWindow);
    m_normalWindow.addTransition(&m_minimize);

    m_restore.setTargetState(&m_normalWindow);
    m_minimizedWindow.addTransition(&m_restore);

    m_stateMachine.start();
}
