/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QEvent>
#include <QObject>
#include <QLineEdit>
#include <QMutex>
#include <QWaitCondition>

struct InputResult
{
    operator bool() const { return not text.isEmpty(); }
    bool isOkPressed;
    QString text;
};

/* These input dialogs work from a different to main thread */
class MyInputDialog : public QObject {

public:
    MyInputDialog(QWidget& w, QString t1, QString t2, QStringList list);
    MyInputDialog(QWidget& w, QString t1, QString t2, QLineEdit::EchoMode type);
    bool event(QEvent* ev) override;
    InputResult result();
private:
    void popUpDialogWindow();
    InputResult m_inputResult;
    QWidget& m_w;
    QString m_t1;
    QString m_t2;
    QStringList m_list;
    bool m_have_list;
    QLineEdit::EchoMode m_type;
    QMutex m_mutex;
    QWaitCondition m_cv;
};
