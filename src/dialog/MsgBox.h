/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QEvent>
#include <QObject>
#include <QMutex>
#include <QWaitCondition>

class QWidget;
/* These input dialogs work from a different to main thread */
class MsgBox : public QObject {
public:
    MsgBox(QWidget& w, QString t1, QString t2 = {}, QString details = {});
    bool event(QEvent* ev) override;
    bool isOkClicked();
private:
    void popUpMsgBox();
    bool m_eventResult;
    QWidget& m_w;
    QString m_t1;
    QString m_t2;
    QString m_details;
    QMutex m_mutex;
    QWaitCondition m_cv;
};
