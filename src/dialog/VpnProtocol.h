#pragma once

#include <QString>

struct VpnProtocol {
    QString name;
    QString prettyName;
    QString description;
};
