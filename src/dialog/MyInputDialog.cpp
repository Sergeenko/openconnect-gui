/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MyInputDialog.h"
#include <QApplication>
#include <QInputDialog>

MyInputDialog::MyInputDialog(QWidget& p_w, QString p_t1, QString p_t2, QStringList p_list)
    : m_w(p_w)
    , m_t1(p_t1)
    , m_t2(p_t2)
    , m_list(p_list)
    , m_have_list(true)
{
    moveToThread(QApplication::instance()->thread());
}

MyInputDialog::MyInputDialog(QWidget& p_w, QString p_t1, QString p_t2, QLineEdit::EchoMode p_type)
    : m_w(p_w)
    , m_t1(p_t1)
    , m_t2(p_t2)
    , m_have_list(false)
    , m_type(p_type)
{
    moveToThread(QApplication::instance()->thread());
}

void MyInputDialog::popUpDialogWindow()
{
    QCoreApplication::postEvent(this, new QEvent(QEvent::User));
}

bool MyInputDialog::event(QEvent* p_ev)
{
    if (p_ev->type() == QEvent::User)
    {
        {
            QMutexLocker lock(&m_mutex);
            if (m_have_list)
            {
                m_inputResult.text = QInputDialog::getItem(&m_w, m_t1, m_t2, m_list, 0, false, &m_inputResult.isOkPressed);
            }
            else
            {
                m_inputResult.text = QInputDialog::getText(&m_w, m_t1, m_t2, m_type, QString(), &m_inputResult.isOkPressed);
            }
        }
        m_cv.wakeOne();
        return true;
    }
    return false;
}

InputResult MyInputDialog::result()
{
    QMutexLocker lock(&m_mutex);
    popUpDialogWindow();
    m_cv.wait(&m_mutex);
    return m_inputResult;
}
