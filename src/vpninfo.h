/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QString>
#include "server_storage.h"
#ifdef _WIN32
#include <winsock2.h>
#else
using SOCKET = int;
#endif

class MainWindow;

class VpnInfo {
public:
    VpnInfo(QString, QString, MainWindow&);
    ~VpnInfo();

    void parse_url();
    int connect() const;
    int dtls_connect() const;
    void openconnectLoop();
    void get_info(QString& dns, QString& ip, QString& ip6);
    void get_cipher_info(QString& cstp, QString& dtls);
    SOCKET get_cmd_fd() const;
    void reset_vpn();
    void set_proxy() const;
    bool get_minimize() const;
    void logVpncScriptOutput();

    unsigned int authgroup_set = 0;
    unsigned int password_set = 0;
    unsigned int form_attempt = 0;
    unsigned int form_pass_attempt = 0;
    mutable QString last_err;
    MainWindow& m_mw;
    StoredServer m_ss;
    struct openconnect_info* m_vpninfo;
private:
    SOCKET cmd_fd;
};
