/*
 * Copyright (C) 2014 Red Hat
 *
 * This file is part of openconnect-gui.
 *
 * openconnect-gui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vpninfo.h"
#include "config.h"
#include "dialog/MsgBox.h"
#include "dialog/MyInputDialog.h"
#include "dialog/mainwindow.h"
#include "gtdb.h"
#include "logger.h"
#include "server_storage.h"
#include <QDir>
#include <QtNetwork/QNetworkProxy>
#include <QtNetwork/QNetworkProxyFactory>
#include <QtNetwork/QNetworkProxyQuery>
extern "C"
{
#include <openconnect.h>
}

static void stats_vfn(void* privdata, const struct oc_stats* stats)
{
    VpnInfo* vpn = static_cast<VpnInfo*>(privdata);
    vpn->m_mw.updateStats(
        stats,
        openconnect_get_dtls_cipher(vpn->m_vpninfo));
}

static void progress_vfn(void* privdata, int level, const char* fmt, ...)
{
    Q_UNUSED(privdata);

    char buf[512];
    size_t len;
    va_list args;

#ifdef NDEBUG
    if (level == PRG_TRACE or level == PRG_DEBUG)
        return;
#else
    /* don't spam */
    if (level == PRG_TRACE)
        return;
#endif

    buf[0] = 0;
    va_start(args, fmt);
    vsnprintf(buf, sizeof(buf), fmt, args);
    va_end(args);

    len = strlen(buf);
    if (buf[len - 1] == '\n')
        buf[len - 1] = 0;
    Logger::instance().addMessage(buf);
}

static int process_auth_form(void* privdata, struct oc_auth_form* form)
{
    VpnInfo* vpn = static_cast<VpnInfo*>(privdata);

    if (form->banner)
        Logger::instance().addMessage(QLatin1String(form->banner));

    if (form->message)
        Logger::instance().addMessage(QLatin1String(form->message));

    if (form->error)
        Logger::instance().addMessage(QLatin1String(form->error));

    QStringList gitems;
    QStringList ditems;
    if (form->authgroup_opt)
    {
        struct oc_form_opt_select* select_opt = form->authgroup_opt;

        for (int i = 0; i < select_opt->nr_choices; i++)
        {
            ditems << select_opt->choices[i]->label;
            gitems << select_opt->choices[i]->name;
        }

        if (select_opt->nr_choices == 1)
        {
            openconnect_set_option_value(
                &select_opt->form,
                select_opt->choices[0]->name);
        }
        else if (gitems.contains(vpn->m_ss.get_groupname()))
        {
            openconnect_set_option_value(
                &select_opt->form,
                vpn->m_ss.get_groupname().toLatin1().data());
        }
        else
        {
            MyInputDialog dialog(
                vpn->m_mw,
                QLatin1String(select_opt->form.name),
                QLatin1String(select_opt->form.label),
                ditems);
            InputResult userInput = dialog.result();

            if (not userInput.isOkPressed)
                return OC_FORM_RESULT_CANCELLED;

            int idx = ditems.indexOf(userInput.text);
            if (idx == -1)
                return OC_FORM_RESULT_CANCELLED;

            openconnect_set_option_value(
                &select_opt->form,
                select_opt->choices[idx]->name);
            userInput.text = QLatin1String(select_opt->choices[idx]->name);

            Logger::instance().addMessage(QLatin1String("Saving group: ") + userInput.text);
            vpn->m_ss.set_groupname(userInput.text);
        }

        if (vpn->authgroup_set == 0)
        {
            vpn->authgroup_set = 1;
            return OC_FORM_RESULT_NEWGROUP;
        }
    }

    for (oc_form_opt* opt = form->opts; opt; opt = opt->next)
    {
        if (opt->flags & OC_FORM_OPT_IGNORE)
            continue;

        if (opt->type == OC_FORM_OPT_SELECT)
        {
            struct oc_form_opt_select* select_opt = reinterpret_cast<oc_form_opt_select*>(opt);

            Logger::instance().addMessage(QLatin1String("Select form: ") + QLatin1String(opt->name));

            if (select_opt == form->authgroup_opt)
            {
                continue;
            }

            QStringList items;
            for (int i = 0; i < select_opt->nr_choices; i++)
            {
                items << select_opt->choices[i]->label;
            }

            MyInputDialog dialog(
                vpn->m_mw,
                QLatin1String(opt->name),
                QLatin1String(opt->label),
                items);
            InputResult userInput = dialog.result();

            if (not userInput.isOkPressed)
                return OC_FORM_RESULT_CANCELLED;

            int idx = ditems.indexOf(userInput.text);
            if (idx == -1)
                return OC_FORM_RESULT_CANCELLED;

            openconnect_set_option_value(opt, select_opt->choices[idx]->name);
        }
        else if (opt->type == OC_FORM_OPT_TEXT)
        {
            Logger::instance().addMessage(QLatin1String("Text form: ") + QLatin1String(opt->name));

            if (not vpn->form_attempt
                and not vpn->m_ss.get_username().isEmpty()
                and strcasecmp(opt->name, "username") == 0)
            {
                openconnect_set_option_value(
                    opt,
                    vpn->m_ss.get_username().toLatin1().data());
                continue;
            }

            InputResult userInput;
            do
            {
                MyInputDialog dialog(
                    vpn->m_mw,
                    QLatin1String(opt->name),
                    QLatin1String(opt->label),
                    QLineEdit::Normal);
                 userInput = dialog.result();

                if (not userInput.isOkPressed)
                    return OC_FORM_RESULT_CANCELLED;
            } while (not userInput);

            if (strcasecmp(opt->name, "username") == 0)
            {
                vpn->m_ss.set_username(userInput.text);
            }

            openconnect_set_option_value(opt, userInput.text.toLatin1().data());
            vpn->form_attempt++;
        }
        else if (opt->type == OC_FORM_OPT_PASSWORD)
        {
            Logger::instance().addMessage(QLatin1String("Password form: ") + QLatin1String(opt->name));

            if (not vpn->form_pass_attempt
                and not vpn->m_ss.get_password().isEmpty()
                and strcasecmp(opt->name, "password") == 0)
            {
                openconnect_set_option_value(opt,
                    vpn->m_ss.get_password().toLatin1().data());
                continue;
            }

            InputResult userInput;
            do
            {
                MyInputDialog dialog(vpn->m_mw, QLatin1String(opt->name),
                    QLatin1String(opt->label),
                    QLineEdit::Password);
                userInput = dialog.result();

                if (not userInput.isOkPressed)
                    return OC_FORM_RESULT_CANCELLED;
            } while (not userInput);

            if (strcasecmp(opt->name, "password") == 0
                and (not vpn->password_set or vpn->form_pass_attempt))
            {
                vpn->m_ss.set_password(userInput.text);
                vpn->password_set = 1;
            }
            openconnect_set_option_value(opt, userInput.text.toLatin1().data());
            vpn->form_pass_attempt++;
        }
        else
        {
            Logger::instance().addMessage(QLatin1String("unknown type ") + QString::number(opt->type) + QString(opt->name) + QString(opt->label));
        }
    } 

    return OC_FORM_RESULT_OK;
}

static int validate_peer_cert(void* privdata, const char* reason)
{
    Q_UNUSED(reason);
    VpnInfo* vpn = static_cast<VpnInfo*>(privdata);
    unsigned char* der = nullptr;
    int der_size = openconnect_get_peer_cert_DER(vpn->m_vpninfo, &der);
    if (der_size <= 0) {
        Logger::instance().addMessage(QObject::tr("Peer's certificate has invalid size!"));
        return -1;
    }

    const char* hash = openconnect_get_peer_cert_hash(vpn->m_vpninfo);
    if (not hash) {
        Logger::instance().addMessage(QObject::tr("Error getting peer's certificate hash"));
        return -1;
    }

    gnutls_datum_t raw;
    raw.data = der;
    raw.size = der_size;

    gtdb tdb(vpn->m_ss);
    int ret = gnutls_verify_stored_pubkey(reinterpret_cast<const char*>(&tdb),
        tdb.tdb, "", "", GNUTLS_CRT_X509, &raw, 0);

    char* details = openconnect_get_peer_cert_details(vpn->m_vpninfo);

    if (ret == GNUTLS_E_NO_CERTIFICATE_FOUND)
    {
        Logger::instance().addMessage(QObject::tr("peer is unknown"));
        MsgBox msgBox(
            vpn->m_mw,
            QObject::tr("You are connecting for the first time to this peer.\n"
                        "You have no guarantee that the server is the computer you think it is.\n\n"
                        "If the information provided below is valid and you trust this host, "
                        "hit 'Ok' to store it and to carry on connecting.\n"
                        "If you do not trust this host, hit 'Cancel' to abandon the connection."),
            QObject::tr("Host: ") + vpn->m_ss.get_servername() + QObject::tr("\n") + hash,
            QString::fromUtf8(details));
        if (not msgBox.isOkClicked())
        {
            return -1;
        }

    }
    else if (ret == GNUTLS_E_CERTIFICATE_KEY_MISMATCH)
    {
        Logger::instance().addMessage(QObject::tr("peer's key has changed!"));
        MsgBox msgBox(
            vpn->m_mw,
            QObject::tr("This peer is known and associated with a different key."
                        "It may be that the server has several keys "
                        "or you are (or were in the past) under MITM attack! "
                        "Do you want to proceed?"),
            QObject::tr("Host: ") + vpn->m_ss.get_servername() + QObject::tr("\n") + hash,
            QString::fromUtf8(details));
        if (not msgBox.isOkClicked())
        {
            return -1;
        }

    }
    else if (ret < 0)
    {
        QString str = QObject::tr("Could not verify certificate: ");
        str += gnutls_strerror(ret);
        Logger::instance().addMessage(str);
        return -1;
    }

    if (details)
    {
        openconnect_free_cert_info(vpn->m_vpninfo, details);
    }
    Logger::instance().addMessage(QObject::tr("saving peer's public key"));
    ret = gnutls_store_pubkey(reinterpret_cast<const char*>(&tdb), tdb.tdb,
        "", "", GNUTLS_CRT_X509, &raw, 0, 0);
    if (ret < 0)
    {
        QString str = QObject::tr("Could not store certificate: ");
        str += gnutls_strerror(ret);
        Logger::instance().addMessage(str);
    }
    else
    {
        vpn->m_ss.save();
    }
    return 0;
}

static int lock_token_vfn(void* privdata)
{
    VpnInfo* vpn = static_cast<VpnInfo*>(privdata);

    openconnect_set_token_mode(vpn->m_vpninfo,
        (oc_token_mode_t)vpn->m_ss.get_token_type(),
        vpn->m_ss.get_token_str().toLatin1().data());

    return 0;
}

static int unlock_token_vfn(void* privdata, const char* newtok)
{
    VpnInfo* vpn = static_cast<VpnInfo*>(privdata);

    vpn->m_ss.set_token_str(newtok);
    vpn->m_ss.save();
    return 0;
}

static void setup_tun_vfn(void* privdata)
{
    VpnInfo* vpn = static_cast<VpnInfo*>(privdata);

    QByteArray vpncScriptFullPath;
    vpncScriptFullPath.append(QCoreApplication::applicationDirPath().toLatin1());
    vpncScriptFullPath.append(QDir::separator().toLatin1());
    vpncScriptFullPath.append(DEFAULT_VPNC_SCRIPT);
    int ret = openconnect_setup_tun_device(vpn->m_vpninfo, vpncScriptFullPath.constData(), NULL);
    if (ret not_eq 0) {
        vpn->last_err = QObject::tr("Error setting up the TUN device");
        return;
    }

    vpn->logVpncScriptOutput();
}

static inline int set_sock_block(int fd)
{
#ifdef _WIN32
    unsigned long mode = 1;
    return ioctlsocket(fd, FIONBIO, &mode);
#else
    return fcntl(fd, F_SETFL, fcntl(fd, F_GETFL) & ~O_NONBLOCK);
#endif
}

VpnInfo::VpnInfo(QString p_name, QString p_profileName, MainWindow& p_mw)
    : m_mw(p_mw)
    , m_ss(std::move(p_profileName))
    , m_vpninfo(
          openconnect_vpninfo_new(
            p_name.toLatin1().data(),
            validate_peer_cert,
            nullptr,
            process_auth_form,
            progress_vfn,
            this))
    , cmd_fd(openconnect_setup_cmd_pipe(m_vpninfo))
{
    if (not m_vpninfo)
    {
        throw std::runtime_error("initial setup fails");
    }

    if (cmd_fd == INVALID_SOCKET)
    {
        Logger::instance().addMessage(QObject::tr("invalid socket"));
        throw std::runtime_error("pipe setup fails");
    }

    int res = set_sock_block(cmd_fd);
    if (res not_eq NO_ERROR)
        Logger::instance().addMessage(QObject::tr("ioctlsocket failed with error: ") + QString::number(res));

    if (not m_ss.get_token_str().isEmpty())
    {
        openconnect_set_token_callbacks(m_vpninfo, this, lock_token_vfn, unlock_token_vfn);
        openconnect_set_token_mode(
            m_vpninfo,
            static_cast<oc_token_mode_t>(m_ss.get_token_type()),
            m_ss.get_token_str().toLatin1().data());
    }
    openconnect_set_stats_handler(m_vpninfo, stats_vfn);
    openconnect_set_protocol(m_vpninfo, m_ss.get_protocol_name());
    openconnect_set_setup_tun_handler(m_vpninfo, setup_tun_vfn);

    if (m_ss.get_proxy())
    {
        set_proxy();
    }
}

VpnInfo::~VpnInfo()
{
    if (m_vpninfo) {
        openconnect_vpninfo_free(m_vpninfo);
    }
}

void VpnInfo::parse_url()
{
    if (openconnect_parse_url(m_vpninfo, const_cast<char*>(m_ss.get_servername().toLocal8Bit().data())))
        throw std::runtime_error("Failed to parse connect URL");
}

int VpnInfo::connect() const
{
    QString cert_file(m_ss.get_cert_file());
    QString ca_file(m_ss.get_ca_cert_file());
    QString key_file(m_ss.get_key_file());

    if (key_file.isEmpty())
        key_file = cert_file;

    if (not cert_file.isEmpty()) {
        openconnect_set_client_cert(m_vpninfo, cert_file.toLatin1().data(),
            key_file.toLatin1().data());
    }

    if (not ca_file.isEmpty()) {
        openconnect_set_system_trust(m_vpninfo, 0);
        openconnect_set_cafile(m_vpninfo, ca_file.toLatin1().data());
    }

#ifdef Q_OS_WIN32
    const QString osName{ "win" };
#elif defined Q_OS_OSX
    const QString osName{ "mac-intel" };
#elif defined Q_OS_LINUX
    const QString osName = QString("linux%1").arg(QSysInfo::buildCpuArchitecture() == "i386" ? "" : "-64").toStdString().c_str();
#elif defined Q_OS_FREEBSD
    const QString osName = QString("freebsd%1").arg(QSysInfo::buildCpuArchitecture() == "i386" ? "" : "-64").toStdString().c_str();
#else
#error Define OS string of other platforms...
#endif
    openconnect_set_reported_os(m_vpninfo, osName.toStdString().c_str());
    int ret = openconnect_obtain_cookie(m_vpninfo);
    if (ret not_eq 0) {
        last_err = QObject::tr("Authentication error; cannot obtain cookie");
        return ret;
    }

    ret = openconnect_make_cstp_connection(m_vpninfo);
    if (ret not_eq 0) {
        last_err = QObject::tr("Error establishing the CSTP channel");
        return ret;
    }

    return 0;
}

int VpnInfo::dtls_connect() const
{
    if (not m_ss.isDtlsDisabled())
    {
        int ret = openconnect_setup_dtls(m_vpninfo,
            m_ss.get_dtls_reconnect_timeout());
        if (ret not_eq 0)
        {
            last_err = QObject::tr("Error setting up DTLS (%1)").arg(ret);
            return ret;
        }
    }

    return 0;
}

void VpnInfo::openconnectLoop()
{
    while (true) {
        int ret = openconnect_mainloop(m_vpninfo,
            m_ss.get_reconnect_timeout(),
            RECONNECT_INTERVAL_MIN);
        if (ret not_eq 0) {
            last_err = QObject::tr("Disconnected");
            logVpncScriptOutput();
            break;
        }
    }
}

void VpnInfo::get_info(QString& dns, QString& ip, QString& ip6)
{
    const struct oc_ip_info* info;
    int ret = openconnect_get_ip_info(m_vpninfo, &info, nullptr, nullptr);
    if (ret == 0) {
        if (info->addr) {
            ip = info->addr;
            if (info->netmask) {
                ip += "/";
                ip += info->netmask;
            }
        }
        if (info->addr6) {
            ip6 = info->addr6;
            if (info->netmask6) {
                ip6 += "/";
                ip6 += info->netmask6;
            }
        }

        dns = info->dns[0];
        if (info->dns[1]) {
            dns += ", ";
            dns += info->dns[1];
        }
        if (info->dns[2]) {
            dns += " ";
            dns += info->dns[2];
        }
    }
    return;
}

void VpnInfo::get_cipher_info(QString& cstp, QString& dtls)
{
    const char* cipher = openconnect_get_cstp_cipher(m_vpninfo);
    if (cipher) {
        cstp = QLatin1String(cipher);
    }
    cipher = openconnect_get_dtls_cipher(m_vpninfo);
    if (cipher) {
        dtls = QLatin1String(cipher);
    }
}

SOCKET VpnInfo::get_cmd_fd() const
{
    return cmd_fd;
}

void VpnInfo::reset_vpn()
{
    openconnect_reset_ssl(m_vpninfo);
    form_pass_attempt = 0;
    password_set = 0;
    authgroup_set = 0;
    form_attempt = 0;
}

void VpnInfo::set_proxy() const
{
    QNetworkProxyFactory::setUseSystemConfiguration(true);
    QNetworkProxyQuery query(QUrl("https://" + m_ss.get_servername()));
    QList<QNetworkProxy> proxies(QNetworkProxyFactory::systemProxyForQuery(query));
    if (proxies.size() > 0 and proxies.at(0).type() not_eq QNetworkProxy::NoProxy)
    {
        QString proxy_type;
        if (proxies.at(0).type() == QNetworkProxy::Socks5Proxy)
            proxy_type = "socks5://";
        else if (proxies.at(0).type() == QNetworkProxy::HttpCachingProxy
                 or proxies.at(0).type() == QNetworkProxy::HttpProxy)
            proxy_type = "http://";

        if (not proxy_type.isEmpty())
        {

            QString str;
            if (proxies.at(0).user() not_eq "0")
            {
                str = proxies.at(0).user() + ":" + proxies.at(0).password() + "@";
            }
            str += proxies.at(0).hostName();
            if (proxies.at(0).port() not_eq 0)
            {
                str += ":" + QString::number(proxies.at(0).port());
            }
            Logger::instance().addMessage(QObject::tr("Setting proxy to: ") + str);
            // FIXME: ...
            openconnect_set_http_proxy(m_vpninfo, str.toLatin1().data());
        }
    }
}

void VpnInfo::logVpncScriptOutput()
{
    /* now read %temp%\\vpnc.log and post it to our log */
    QString tfile = QDir::tempPath() + QDir::separator() + QLatin1String("vpnc.log");
    QFile file(tfile);
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream in(&file);

        QString bannerMessage;
        bool processBannerMessage = false;

        while (!in.atEnd()) {
            const QString line{ in.readLine() };
            Logger::instance().addMessage(line);

            if (line == QLatin1String("--------------------- BANNER ---------------------")) {
                processBannerMessage = true;
                continue;
            }
            if (line == QLatin1String("------------------- BANNER end -------------------")) {
                processBannerMessage = false;
                continue;
            }
            if (processBannerMessage) {
                bannerMessage += line + "\n";
            }
        }
        file.close();
        if (not file.remove()) {
            Logger::instance().addMessage(QLatin1String("Could not remove ") + tfile + ": " + QString::number((int)file.error()));
        }

        if (not m_ss.get_batch_mode() and not bannerMessage.isEmpty()) {
            // TODO: msgbox title; e.g. Accept/Continue + Disconnect on buttons
            MsgBox msgBox(
                m_mw,
                bannerMessage);
            if (not msgBox.isOkClicked()) {
                m_mw.on_disconnectClicked();
            }
        }
    } else {
        Logger::instance().addMessage(QLatin1String("Could not open ") + tfile + ": " + QString::number((int)file.error()));
    }
}
