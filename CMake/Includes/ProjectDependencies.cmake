# common dependenceis
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt6 6.2 REQUIRED COMPONENTS Core Gui Widgets Network StateMachine)

if(MINGW)
# Qt6 workaround for broken cmake targets for plugins (track https://bugreports.qt.io/browse/QTBUG-94066)
    find_package(Qt6QWindowsIntegrationPlugin PATHS ${Qt6Gui_DIR})
    find_package(Qt6QWindowsVistaStylePlugin PATHS ${Qt6Widgets_DIR})
#
   get_target_property(_qwindows_dll Qt6::QWindowsIntegrationPlugin LOCATION)
   get_target_property(_qwinstyle_dylib Qt6::QWindowsVistaStylePlugin LOCATION)
endif()
if(APPLE)
# Qt6 workaround for broken cmake targets for plugins (track https://bugreports.qt.io/browse/QTBUG-94066)
    find_package(Qt6QCocoaIntegrationPlugin PATHS ${Qt6Gui_DIR})
    find_package(Qt6QMacStylePlugin PATHS ${Qt6Widgets_DIR})
#
    get_target_property(_qcocoa_dylib Qt6::QCocoaIntegrationPlugin LOCATION)
    get_target_property(_qmacstyle_dylib Qt6::QMacStylePlugin LOCATION)
endif()

# On MacOS, Linux or when cross-building for Windows we have sane
# package management. Use it.
if(CMAKE_CROSSCOMPILING OR NOT WIN32)
    find_package(GnuTLS REQUIRED)
    if(GNUTLS_FOUND)
        message(STATUS "Library 'GnuTLS' found at ${GNUTLS_LIBRARIES}")
    include_directories(SYSTEM ${GNUTLS_INCLUDE_DIR})
    else()
        message(FATAL_ERROR "Library 'GnuTLS' not found! Install it vie e.g. 'brew install gnutls' or 'dnf install gnutls-devel'")
    endif()

    find_package(OpenConnect REQUIRED)
    if(OPENCONNECT_FOUND)
        message(STATUS "Library 'OpenConnect' found at ${OPENCONNECT_LIBRARIES}")
        link_directories(${OPENCONNECT_LIBRARY_DIRS})
        include_directories(SYSTEM ${OPENCONNECT_INCLUDE_DIRS})
    else()
        message(FATAL_ERROR "Libraru 'OpenConnect' not found! Install it vie e.g. 'brew install openconnect or 'dnf install openconnect'")
    endif()

    # This is optional as the package isn't ubiquitous. We'll pull it
    # in and build it locally if not found.
    find_package(spdlog CONFIG)
endif()

if(UNIX)
    set(CMAKE_THREAD_PREFER_PTHREAD ON)
    find_package(Threads REQUIRED)
endif()

if(APPLE)
    find_library(SECURITY_LIBRARY Security REQUIRED)
    if(SECURITY_LIBRARY)
        message(STATUS "Framework 'Security' found at ${SECURITY_LIBRARY}")

        link_directories(${SECURITY_LIBRARY_DIRS})
        include_directories(SYSTEM ${SECURITY_LIBRARY_INCLUDE_DIRS})
    else()
        message(FATAL_ERROR "Framework 'Security' not found!")
    endif()
    mark_as_advanced(SECURITY_LIBRARY)
endif()

# mingw32/mingw64 and other external dependencies
include(ProjectExternals)
